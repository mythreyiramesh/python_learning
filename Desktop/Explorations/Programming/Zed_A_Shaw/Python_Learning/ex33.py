max = int(raw_input("Loop till? > "))
incr = int(raw_input("Increment by? > "))
numbers = []

def while_looping(max, incr):
	i = 0
	while i < max:
		print "At the top i is %d" % i
		numbers.append(i)

		i = i + incr
		print "The numbers are now ", numbers
		print "At the bottom i is %d" % i

	print "The numbers: "
	for num in numbers:
		print num

print "This will loop till your maximum"
while_looping(max, incr)
